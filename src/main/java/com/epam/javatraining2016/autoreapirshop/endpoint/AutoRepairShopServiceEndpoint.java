package com.epam.javatraining2016.autoreapirshop.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;

@WebService(serviceName = "AutoRepairShopService")
public class AutoRepairShopServiceEndpoint {

  @Autowired
  private AutoRepairShopService service;

  @WebMethod
  public String getVersion() {
    return service.getVersion();
  }

  @WebMethod
  public int createClient(@WebParam(name = "name") @XmlElement(required = true) String name) {
    return service.createClient(name);
  }

  @WebMethod
  public Client getClient(@WebParam(name = "id") @XmlElement(required = true) int clientId) {
    return service.getClient(clientId);
  }

  @WebMethod
  public int createComment(@WebParam(name = "order_id") @XmlElement(required = true) int orderId,
      @WebParam(name = "comment_text") @XmlElement(required = true) String commentText) {
    return service.createComment(orderId, commentText);
  }

  public Comment getComment(
      @WebParam(name = "comment_id") @XmlElement(required = true) int commentId) {
    return service.getComment(commentId);
  }
  
  @WebMethod
  public int createOrder(@WebParam(name = "client_id") @XmlElement(required = true) int clientId) {
    return service.createOrder(clientId);
  }

  @WebMethod
  public Order getOrder(@WebParam(name = "id") @XmlElement(required = true) int id) {
    return service.getOrder(id);
  }

  
  @WebMethod
  public Order[] getOrders(
      @WebParam(name = "client_id") @XmlElement(required = true) int clientId) {
    return service.getOrdersForClient(clientId);
  }

  @WebMethod
  public int changeOrderStatus(@WebParam(name = "order_id") @XmlElement(required = true) int id,
      @WebParam(name = "status") @XmlElement(required = true) String statusName) {
    return service.changeOrderStatus(id, statusName);
  }
}
