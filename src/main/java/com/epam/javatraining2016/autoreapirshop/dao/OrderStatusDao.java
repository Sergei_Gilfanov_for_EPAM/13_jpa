package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderStatusRow;

@Repository
public class OrderStatusDao {
  @PersistenceContext
  private EntityManager em;

  public OrderStatusRow get(int orderStatusId) {
    OrderStatusRow retval = em.find(OrderStatusRow.class, orderStatusId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public OrderStatusRow findByName(String statusName) {
    OrderStatusRow retval =
        em.createQuery("FROM OrderStatusRow status WHERE status.statusName = :statusName",
            OrderStatusRow.class).setParameter("statusName", statusName).getSingleResult();
    return retval;
  }

  public OrderStatusRow getCurrentStatus(OrderRow order) {
    OrderHistoryRow orderHistory = order.getOrderHistory();
    // Не должно выдавать исключение по пустому результату, т.к мы при создании заказа всегда
    // записываем первый элемент
    // истории его статуса
    OrderStatusRow retval = em
        .createQuery(
            "SELECT historyRecord.orderStatus FROM OrderHistoryRecordStatusChangeRow historyRecord "
                + "WHERE historyRecord.orderHistory.id = :historyRecordId ORDER BY historyRecord.id DESC",
            OrderStatusRow.class)
        .setParameter("historyRecordId", orderHistory.getId()).setFirstResult(0).setMaxResults(1)
        .getSingleResult();
    return retval;
  }
}
