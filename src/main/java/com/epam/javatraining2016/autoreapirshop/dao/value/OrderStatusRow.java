package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.ReadOnly;

@ReadOnly
@Entity
@Table(name="OrderStatus")
public class OrderStatusRow {
  @Id
  private int id;
  
  @Column(name="status_name")
  private String statusName;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getStatusName() {
    return statusName;
  }
  public void setStatusName(String statusName) {
    this.statusName = statusName;
  }
}
