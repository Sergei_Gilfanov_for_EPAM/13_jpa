package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.InvoiceListRow;

@Repository
public class InvoiceListDao {
  @PersistenceContext
  private EntityManager em;

  public InvoiceListRow create() {
    InvoiceListRow retval = new InvoiceListRow();
    em.persist(retval);
    return retval;
  }

  public InvoiceListRow get(int invoiceListId) {
    InvoiceListRow retval = em.find(InvoiceListRow.class, invoiceListId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }
}
