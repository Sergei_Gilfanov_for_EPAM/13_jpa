package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.CommentRow;

@Repository
public class CommentDao {
  @PersistenceContext
  private EntityManager em;

  public CommentRow create(String commentText) {
    CommentRow retval = new CommentRow();
    retval.setCommentText(commentText);
    em.persist(retval);
    return retval;
  }

  public CommentRow get(int commentId) {
    CommentRow retval = em.find(CommentRow.class, commentId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }
}
