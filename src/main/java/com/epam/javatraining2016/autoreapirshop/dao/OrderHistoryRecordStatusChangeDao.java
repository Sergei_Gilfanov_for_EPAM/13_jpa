package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.CommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRecordStatusChangeRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderStatusRow;

@Repository
public class OrderHistoryRecordStatusChangeDao {
  @PersistenceContext
  private EntityManager em;

  public OrderHistoryRecordStatusChangeRow create(OrderHistoryRow orderHistory,
      OrderStatusRow orderStatus, CommentRow comment) {
    OrderHistoryRecordStatusChangeRow retval = new OrderHistoryRecordStatusChangeRow();
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setOrderStatus(orderStatus);
    retval.setComment(comment);
    em.persist(retval);
    return retval;
  }

  @Cacheable("OrderHistoryRecordStatusChange")
  public OrderHistoryRecordStatusChangeRow getShallow(int recordId) {
    OrderHistoryRecordStatusChangeRow retval =
        em.find(OrderHistoryRecordStatusChangeRow.class, recordId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }
}
