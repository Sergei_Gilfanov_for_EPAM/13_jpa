package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRow;

@Repository
public class OrderHistoryDao {
  @PersistenceContext
  private EntityManager em;
  
  public OrderHistoryRow create() {
    OrderHistoryRow retval = new OrderHistoryRow();
    em.persist(retval);
    return retval;
  }

  public OrderHistoryRow get(int orderHistoryId) {
    OrderHistoryRow retval = em.find(OrderHistoryRow.class, orderHistoryId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
