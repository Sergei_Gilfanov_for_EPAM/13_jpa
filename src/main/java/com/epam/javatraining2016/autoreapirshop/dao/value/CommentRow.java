package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Comment")
public class CommentRow {
  
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="comment_id_seq")
  @SequenceGenerator(name="comment_id_seq", sequenceName="comment_id_seq", allocationSize=1)
  private int id;
  
  @Column(name="comment_text")
  private String commentText;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getCommentText() {
    return commentText;
  }
  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }
  
}
