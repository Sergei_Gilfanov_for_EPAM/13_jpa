package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@DiscriminatorValue(value="2")
@Table(name="OrderHistoryRecordComment")
public class OrderHistoryRecordCommentRow extends OrderHistoryRecordRow {

  @OneToOne
  @JoinColumn(name="comment_id")
  private CommentRow comment;
  
  public CommentRow getComment() {
    return comment;
  }
  public void setComment(CommentRow comment) {
    this.comment = comment;
  }
}
