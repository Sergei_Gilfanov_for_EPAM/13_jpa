package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "record_type",discriminatorType=DiscriminatorType.INTEGER)
@Table(name = "OrderHistoryRecord")
public abstract class OrderHistoryRecordRow {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderhistoryrecord_id_seq")
  @SequenceGenerator(name = "orderhistoryrecord_id_seq", sequenceName = "orderhistoryrecord_id_seq",
      allocationSize = 1)
  private int id;

  @OneToOne
  @JoinColumn(name = "order_history_id")
  private OrderHistoryRow orderHistory;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public OrderHistoryRow getOrderHistory() {
    return orderHistory;
  }

  public void setOrderHistory(OrderHistoryRow orderHistory) {
    this.orderHistory = orderHistory;
  }
}
