package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Client")
public class ClientRow {
  
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="client_id_seq")
  @SequenceGenerator(name="client_id_seq", sequenceName="client_id_seq", allocationSize=1)
  private int id;

  @Column(name="client_name")
  private String clientName;
  
  @OneToOne
  @JoinColumn(name="order_list_id")
  private OrderListRow orderList;
  
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  
  public String getClientName() {
    return clientName;
  }
  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
  
  public OrderListRow getOrderList() {
    return orderList;
  }
  public void setOrderList(OrderListRow orderList) {
    this.orderList = orderList;
  }
  
}
