package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@DiscriminatorValue(value="1")
@Table(name="OrderHistoryRecordStatusChange")
public class OrderHistoryRecordStatusChangeRow extends OrderHistoryRecordRow {

  @OneToOne
  @JoinColumn(name="status_id")
  private OrderStatusRow orderStatus;

  @OneToOne
  @JoinColumn(name="comment_id")
  private CommentRow comment;
  
  public OrderStatusRow getOrderStatus() {
    return orderStatus;
  }
  public void setOrderStatus(OrderStatusRow orderStatus) {
    this.orderStatus = orderStatus;
  }
  public CommentRow getComment() {
    return comment;
  }
  public void setComment(CommentRow comment) {
    this.comment = comment;
  }
}
