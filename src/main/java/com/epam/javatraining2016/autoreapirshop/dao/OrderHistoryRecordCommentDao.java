package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.CommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRecordCommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRow;

@Repository
public class OrderHistoryRecordCommentDao {
  @PersistenceContext
  private EntityManager em;  

  public OrderHistoryRecordCommentRow create(OrderHistoryRow orderHistory, CommentRow comment) {
    OrderHistoryRecordCommentRow retval = new OrderHistoryRecordCommentRow();
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setComment(comment);
    em.persist(retval);
    return retval;
  }

  @Cacheable("OrderHistoryComment")
  public OrderHistoryRecordCommentRow getShallow(int recordId) {
    OrderHistoryRecordCommentRow retval = em.find(OrderHistoryRecordCommentRow.class, recordId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }
}
