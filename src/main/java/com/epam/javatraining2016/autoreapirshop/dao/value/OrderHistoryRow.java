package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="OrderHistory")
public class OrderHistoryRow {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="orderhistory_id_seq")
  @SequenceGenerator(name="orderhistory_id_seq", sequenceName="orderhistory_id_seq", allocationSize=1)  
  private int id;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
