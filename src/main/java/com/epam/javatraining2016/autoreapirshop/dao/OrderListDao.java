package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.OrderListRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderRow;

@Repository
public class OrderListDao {
  @PersistenceContext
  private EntityManager em;

  public OrderListRow create() {
    OrderListRow retval = new OrderListRow();
    em.persist(retval);
    return retval;
  }

  public OrderListRow get(int orderListId) {
    OrderListRow retval = em.find(OrderListRow.class, orderListId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }

  public void add(OrderListRow orderList, OrderRow order) {
    orderList.add(order);
    em.persist(orderList);
  }
}
