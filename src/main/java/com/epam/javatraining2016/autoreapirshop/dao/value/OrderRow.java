package com.epam.javatraining2016.autoreapirshop.dao.value;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Orders")
public class OrderRow {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="orders_id_seq")
  @SequenceGenerator(name="orders_id_seq", sequenceName="orders_id_seq", allocationSize=1)
  private int id;
  
  @OneToOne
  @JoinColumn(name="invoice_list_id")
  private InvoiceListRow invoiceList;
  
  @OneToOne
  @JoinColumn(name="order_history_id")
  private OrderHistoryRow orderHistory;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public InvoiceListRow getInvoiceList() {
    return invoiceList;
  }
  public void setInvoiceList(InvoiceListRow invoiceList) {
    this.invoiceList = invoiceList;
  }
  public OrderHistoryRow getOrderHistory() {
    return orderHistory;
  }
  public void setOrderHistory(OrderHistoryRow orderHistory) {
    this.orderHistory = orderHistory;
  }
  
}
