package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.InvoiceListRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderHistoryRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderRow;

@Repository
public class OrderDao {
  @PersistenceContext
  private EntityManager em;

  public OrderRow create(InvoiceListRow invoiceList, OrderHistoryRow orderHistory) {
    OrderRow retval = new OrderRow();
    retval.setInvoiceList(invoiceList);
    retval.setOrderHistory(orderHistory);
    em.persist(retval);
    return retval;
  }

  public OrderRow getShallow(int orderId) {
    OrderRow retval = em.find(OrderRow.class, orderId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }

}
