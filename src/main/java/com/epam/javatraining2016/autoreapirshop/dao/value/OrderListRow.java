package com.epam.javatraining2016.autoreapirshop.dao.value;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="OrderList")
public class OrderListRow implements List<OrderRow> {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="orderlist_id_seq")
  @SequenceGenerator(name="orderlist_id_seq", sequenceName="orderlist_id_seq", allocationSize=1)
  private int id;
  
  @ManyToMany
  @JoinTable(
      name="OrderListOrders",
      joinColumns=@JoinColumn(name="order_list_id", referencedColumnName="id"),
          inverseJoinColumns=@JoinColumn(name="order_id", referencedColumnName="id")
      )
  private List<OrderRow> content;
  

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<OrderRow> getContent() {
    return content;
  }

  public void setContent(List<OrderRow> content) {
    this.content = content;
  }

  // delegate List<OrderRow> interface to 'content' field
  public void forEach(Consumer<? super OrderRow> action) {
    content.forEach(action);
  }

  public int size() {
    return content.size();
  }

  public boolean isEmpty() {
    return content.isEmpty();
  }

  public boolean contains(Object o) {
    return content.contains(o);
  }

  public Iterator<OrderRow> iterator() {
    return content.iterator();
  }

  public Object[] toArray() {
    return content.toArray();
  }

  public <T> T[] toArray(T[] a) {
    return content.toArray(a);
  }

  public boolean add(OrderRow e) {
    return content.add(e);
  }

  public boolean remove(Object o) {
    return content.remove(o);
  }

  public boolean containsAll(Collection<?> c) {
    return content.containsAll(c);
  }

  public boolean addAll(Collection<? extends OrderRow> c) {
    return content.addAll(c);
  }

  public boolean addAll(int index, Collection<? extends OrderRow> c) {
    return content.addAll(index, c);
  }

  public boolean removeAll(Collection<?> c) {
    return content.removeAll(c);
  }

  public boolean retainAll(Collection<?> c) {
    return content.retainAll(c);
  }

  public void replaceAll(UnaryOperator<OrderRow> operator) {
    content.replaceAll(operator);
  }

  public boolean removeIf(Predicate<? super OrderRow> filter) {
    return content.removeIf(filter);
  }

  public void sort(Comparator<? super OrderRow> c) {
    content.sort(c);
  }

  public void clear() {
    content.clear();
  }

  public boolean equals(Object o) {
    return content.equals(o);
  }

  public int hashCode() {
    return content.hashCode();
  }

  public OrderRow get(int index) {
    return content.get(index);
  }

  public OrderRow set(int index, OrderRow element) {
    return content.set(index, element);
  }

  public void add(int index, OrderRow element) {
    content.add(index, element);
  }

  public Stream<OrderRow> stream() {
    return content.stream();
  }

  public OrderRow remove(int index) {
    return content.remove(index);
  }

  public Stream<OrderRow> parallelStream() {
    return content.parallelStream();
  }

  public int indexOf(Object o) {
    return content.indexOf(o);
  }

  public int lastIndexOf(Object o) {
    return content.lastIndexOf(o);
  }

  public ListIterator<OrderRow> listIterator() {
    return content.listIterator();
  }

  public ListIterator<OrderRow> listIterator(int index) {
    return content.listIterator(index);
  }

  public List<OrderRow> subList(int fromIndex, int toIndex) {
    return content.subList(fromIndex, toIndex);
  }

  public Spliterator<OrderRow> spliterator() {
    return content.spliterator();
  }
  
}
