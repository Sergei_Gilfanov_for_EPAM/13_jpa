package com.epam.javatraining2016.autoreapirshop.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.epam.javatraining2016.autoreapirshop.dao.value.ClientRow;
import com.epam.javatraining2016.autoreapirshop.dao.value.OrderListRow;

@Repository
public class ClientDao {
  @PersistenceContext
  private EntityManager em;

  public ClientRow create(String name, OrderListRow orderList) {
    ClientRow retval = new ClientRow();
    retval.setClientName(name);
    retval.setOrderList(orderList);
    em.persist(retval);
    return retval;
  }

  public ClientRow getShallow(int clientId) {
    ClientRow retval = em.find(ClientRow.class, clientId);
    if (retval == null) {
      throw new NoResultException();
    }
    return retval;
  }
}
