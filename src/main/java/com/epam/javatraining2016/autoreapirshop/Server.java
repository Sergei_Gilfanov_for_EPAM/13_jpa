package com.epam.javatraining2016.autoreapirshop;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.eclipse.persistence.config.BatchWriting;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;

@SpringBootApplication
@EnableTransactionManagement
public class Server extends JpaBaseConfiguration {

  @Bean
  DataSource dataSource(@Value("${jdbc.url}") String url, @Value("${jdbc.user}") String username,
      @Value("${jdbc.password}") String password) {
    DriverManagerDataSource retval = new DriverManagerDataSource();
    retval.setDriverClassName("org.postgresql.Driver");
    retval.setUrl(url);
    retval.setUsername(username);
    retval.setPassword(password);
    return retval;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      final EntityManagerFactoryBuilder builder, DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean retval = builder.dataSource(dataSource)
        .packages("com.epam.javatraining2016.autoreapirshop.dao.value")
        .properties(getVendorProperties()).build();
    return retval;
  }


  @Bean
  public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
    final JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf);
    return transactionManager;
  }

  @Override
  protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
    EclipseLinkJpaVendorAdapter retval = new EclipseLinkJpaVendorAdapter();
    return retval;
  }

  @Override
  protected Map<String, Object> getVendorProperties() {
    final Map<String, Object> ret = new HashMap<>();
    ret.put(PersistenceUnitProperties.BATCH_WRITING, BatchWriting.JDBC);
    ret.put(PersistenceUnitProperties.WEAVING, "false");
    // ret.put("eclipselink.logging.level.sql", "FINE");
    // ret.put("eclipselink.logging.parameters", "true");
    return ret;
  }

  @Bean
  AutoRepairShopServiceEndpoint autoRepairShopServiceEndpoint() {
    return new AutoRepairShopServiceEndpoint();
  }

  @Bean
  SimpleJaxWsServiceExporter simpleJaxWsServiceExporter() {
    SimpleJaxWsServiceExporter retval = new SimpleJaxWsServiceExporter();
    retval.setBaseAddress("http://localhost:8080/");
    return retval;
  }

  public static void main(String[] args) throws InterruptedException {
    SpringApplication application = new SpringApplication(Server.class);
    // Нам не нужен встроенный tomcat, SimpleJaxWsServiceExporter использует собственный http сервер
    application.setWebEnvironment(false);
    application.run(args);
  }

}
